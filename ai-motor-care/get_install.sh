#!/usr/bin/env bash
#
#           AI Motor Care Installer Script
#
#   GitLab: https://gitlab.com/daliworks/ai-motor-care
#   Issues: https://gitlab.com/daliworks/ai-motor-care/-/issues
#   Requires: bash, mv, rm, tr, type, grep, sed, curl/wget, tar (or unzip on OSX and Windows)
#
#   This script installs AI Motor Care to your path.
#   Usage:
#
#   	$ curl -fsSL https://ai-motor-care.xyz/get.sh | bash
#   	  or
#   	$ wget -qO- https://ai-motor-care.xyz/get.sh | bash
#
#   In automated environments, you may want to run as root.
#   If using curl, we recommend using the -fsSL flags.
#
#   This should work on Mac, Linux, and BSD systems, and
#   hopefully Windows with Cygwin. Please open an issue if
#   you notice any bugs.
#

install_ai_motor_care()
{
	trap 'echo -e "Aborted, error $? in command: $BASH_COMMAND"; trap ERR; return 1' ERR
	ai_motor_care_os="unsupported"
	ai_motor_care_arch="unknown"
	install_path="/usr/local/bin"
  library_path="/usr/local/lib"

	# Termux on Android has $PREFIX set which already ends with /usr
	if [[ -n "$ANDROID_ROOT" && -n "$PREFIX" ]]; then
		install_path="$PREFIX/bin"
	fi

	# Fall back to /usr/bin if necessary
	if [[ ! -d $install_path ]]; then
		install_path="/usr/bin"
	fi

	# Not every platform has or needs sudo (https://termux.com/linux.html)
	((EUID)) && [[ -z "$ANDROID_ROOT" ]] && sudo_cmd="sudo"

	#########################
	# Which OS and version? #
	#########################
	ai_motor_care_dir="ai-motor-care"
	ai_motor_care_bin="ai-motor-care"
	ai_motor_care_config="ai-motor-care.config"
	ai_motor_care_dl_ext=".tar.gz"

	# NOTE: `uname -m` is more accurate and universal than `arch`
	# See https://en.wikipedia.org/wiki/Uname
	unamem="$(uname -m)"
	case $unamem in
	*aarch64*)
		ai_motor_care_arch="arm64";;
  *arm64*)
		ai_motor_care_arch="arm64";;
	*64*)
		ai_motor_care_arch="amd64";;
	*)
		echo "Aborted, unsupported or unknown architecture: $unamem"
		return 2
		;;
	esac

	unameu="$(tr '[:lower:]' '[:upper:]' <<<$(uname))"
	if [[ $unameu == *DARWIN* ]]; then
		ai_motor_care_os="darwin"
	elif [[ $unameu == *LINUX* ]]; then
		ai_motor_care_os="linux"
	else
		echo "Aborted, unsupported or unknown OS: $uname"
		return 6
	fi

	########################
	# Download and extract #
	########################

	echo "Downloading AI Motor Care for $ai_motor_care_os/$ai_motor_care_arch..."
	if type -p curl >/dev/null 2>&1; then
		net_getter="curl -fSL"
	elif type -p wget >/dev/null 2>&1; then
		net_getter="wget -qO-"
	else
		echo "Aborted, could not find curl or wget"
		return 7
	fi

	ai_motor_care_file="${ai_motor_care_os}-$ai_motor_care_arch-ai-motor-care$ai_motor_care_dl_ext"
	ai_motor_care_url="https://s3.ap-northeast-1.amazonaws.com/thingplus.service/ntek/ai-motor-care/$ai_motor_care_file"
	# Use $PREFIX for compatibility with Termux on Android
	rm -rf "$PREFIX/tmp/$ai_motor_care_file"

	${net_getter} "$ai_motor_care_url" > "$PREFIX/tmp/$ai_motor_care_file"

	echo "Extracting..."
	case "$ai_motor_care_file" in
		*.zip)    unzip -o "$PREFIX/tmp/$ai_motor_care_file" "$ai_motor_care_dir" -d "$PREFIX/tmp/" ;;
		*.tar.gz) tar -xvzf "$PREFIX/tmp/$ai_motor_care_file" -C "$PREFIX/tmp/" "$ai_motor_care_dir";;
	esac
	chmod +x "$PREFIX/tmp/$ai_motor_care_dir/bin/$ai_motor_care_bin"
  chmod +wx "$PREFIX/tmp/$ai_motor_care_dir/lib/$ai_motor_care_dir/$ai_motor_care_config"

	echo "Putting ai_motor_care in $install_path (may require password)"
	$sudo_cmd mv "$PREFIX/tmp/$ai_motor_care_dir/bin/$ai_motor_care_bin" "$install_path/$ai_motor_care_bin"
  $sudo_cmd mv "$PREFIX/tmp/$ai_motor_care_dir/lib/$ai_motor_care_dir" "$library_path/$ai_motor_care_dir"
	if setcap_cmd=$(PATH+=$PATH:/sbin type -p setcap); then
		$sudo_cmd $setcap_cmd cap_net_bind_service=+ep "$install_path/$ai_motor_care_dir"
	fi
  $sudo_cmd rm -r "$PREFIX/tmp/$ai_motor_care_dir"
	$sudo_cmd rm -- "$PREFIX/tmp/$ai_motor_care_file"

	if type -p $ai_motor_care_dir >/dev/null 2>&1; then
		echo "Successfully installed"
		trap ERR
		return 0
	else
		echo "Something went wrong, AI Motor Care is not in your path"
		trap ERR
		return 1
	fi
}

install_ai_motor_care