#Requires -RunAsAdministrator 
#
#           AI Motor Care Installer Script
#
#   GitLab: https://gitlab.com/daliworks/ai-motor-care
#   Issues: https://gitlab.com/daliworks/ai-motor-care/-/issues
#
#   Usage:
#
#   	iwr -useb https://ai-motor-care.xyz/get.ps1 | iex
#
#	You should run it as administrator so it can add ai-motor-care to 
#	the PATH.
#
function Install-AI-Motor-Care {
	$ErrorActionPreference = "Stop"

	$arch = "amd64"

	If ((Get-WmiObject Win32_OperatingSystem).OSArchitecture -eq "64-bit") {
		$arch = "amd64"
	}

	$file = "windows-$arch-ai-motor-care.zip"
	$url = "https://s3.ap-northeast-1.amazonaws.com/thingplus.service/ntek/ai-motor-care/$file"
	$temp =  New-TemporaryFile
	$folder = "${env:ProgramFiles}\ai-motor-care"

	Write-Host "Downloading" $url
		$WebClient = New-Object System.Net.WebClient 
		$WebClient.DownloadFile( $url, $temp ) 
	
	Write-Host "Extracting $temp.zip"
		Move-Item $temp "$temp.zip"
		Expand-Archive "$temp.zip" -DestinationPath $temp

	Write-Host "Installing ai-motor-care on $folder"
		If (-not (Test-Path $folder)) {
			New-Item -ItemType "directory" -Path $folder | Out-Null
		}
		if (Test-Path "$folder\ai-motor-care.exe") {
			Write-Host "Removing the old version"
			Remove-Item -Force "$folder\ai-motor-care.exe"
		}
		Move-Item "$temp\bin\ai-motor-care.exe" "$folder\bin\ai-motor-care.exe"
		Move-Item "$temp\lib\ai-motor-care" "$folder\lib\ai-motor-care"

	Write-Host "Cleaning temporary files"
		Remove-Item -Force "$temp.zip"
		Remove-Item -Force -Recurse "$temp"

	Write-Host "Adding ai-motor-care to the PATH"
		if ((Get-Command "pandoc.exe" -ErrorAction SilentlyContinue) -eq $null) { 
			$path = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).Path
			$path = $path + ";$folder\bin"
			Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value "$path"
		}

	Write-Host "ai-motor-care successfully installed!" -ForegroundColor Green 
}

Install-AI-Motor-Care